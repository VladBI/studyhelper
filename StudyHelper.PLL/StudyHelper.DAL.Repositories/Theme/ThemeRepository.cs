﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using StudyHelper.BLL.Interfaces.Models.DataObjects.Theme;
using StudyHelper.BLL.Interfaces.Repositories;
using StudyHelper.DAL.Context.Context;
using StudyHelper.DAL.Context.Models;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace StudyHelper.DAL.Repositories.Theme
{
    public class ThemeRepository: IThemeRepository
    {
        private readonly StudyHelperContext _context;

        private readonly IMapper _mapper;

        public ThemeRepository(StudyHelperContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<int> AddThemeAsync(AddThemeDataObject dataObject)
        {
            var themeDbo = _mapper.Map<ThemeDbo>(dataObject);

            _context.Themes.Add(themeDbo);
            
            await _context.SaveChangesAsync();

            return themeDbo.Id;
        }

        public async Task<IEnumerable<SimpleThemeDataObject>> GetProjectsThemes(int id)
        {
            var themesQuery = _context.Themes
                .Where(th => th.ProjectId == id)
                .Select(th => new SimpleThemeDataObject
                {
                    Id = th.Id,
                    Name = th.Name,
                    Description = th.Description
                });

            var themes = await themesQuery.ToListAsync();

            return themes;
        }

        public async Task<ThemeMainInfoDataObject> GetConcreteTheme(int id)
        {
            var themeQuery = _context.Themes
                .Select(th => new ThemeMainInfoDataObject
                {
                    Id = th.Id,
                    Name = th.Name,
                    Description = th.Description,
                    ProjectId = th.ProjectId
                });

            var theme = await themeQuery.FirstOrDefaultAsync(th => th.Id == id);

            theme.QuestionsCount = await _context.Questions.CountAsync(th => th.Id == id);

            return theme;
        }
    }
}
