﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using StudyHelper.BLL.Interfaces.Models.DataObjects.Answer;
using StudyHelper.BLL.Interfaces.Repositories;
using StudyHelper.DAL.Context.Context;
using StudyHelper.DAL.Context.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudyHelper.DAL.Repositories.Answer
{
    public class AnswerRepository : IAnswerRepository
    {
        private readonly StudyHelperContext _context;

        private readonly IMapper _mapper;

        public AnswerRepository(IMapper mapper, StudyHelperContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        public async Task<int> AddAnswerAsync(AddAnswerDataObject dataObject)
        {
            var answerDbo = _mapper.Map<AnswerDbo>(dataObject);

            _context.Answers.Add(answerDbo);

            await _context.SaveChangesAsync();

            return answerDbo.Id;
        }

        public async Task<IEnumerable<SimpleAnswerDataObject>> GetAllThemesAnswers(int themeId)
        {
            var answersQuery = _context.Answers
                                   .Include(a => a.Questions)
                                   .ThenInclude(aq => aq.Question)
                .Where(a => a.Questions.Any(t => t.Question.ThemeId == themeId) || a.IsShared)
                .Select(a => new SimpleAnswerDataObject
                {
                    Id = a.Id,
                    Title = a.Title,
                    QuestionTypes = a.QuestionTypes
                });

            var answersList = await answersQuery.ToListAsync();

            return answersList;
        }

        }
}
