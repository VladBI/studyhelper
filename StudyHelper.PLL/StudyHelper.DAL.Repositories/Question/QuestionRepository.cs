﻿using AutoMapper;
using StudyHelper.BLL.Interfaces.Models.DataObjects.Question;
using StudyHelper.BLL.Interfaces.Repositories;
using StudyHelper.DAL.Context.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using StudyHelper.DAL.Context.Models;
using StudyHelper.DAL.Context.Models.ManyToMany;

namespace StudyHelper.DAL.Repositories.Question
{
    public class QuestionRepository: IQuestionRepository
    {
        private readonly StudyHelperContext _context;

        private readonly IMapper _mapper;

        public QuestionRepository(IMapper mapper, StudyHelperContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        public async Task<int> AddQuestionAsync(AddQuestionDataObject dataObject)
        {
            var questionDbo = _mapper.Map<QuestionDbo>(dataObject);
            
            _context.Questions.Add(questionDbo);

            await _context.SaveChangesAsync();

            var answerDbo = await _context.Answers.FirstOrDefaultAsync(a => a.Id == dataObject.AnswerId);
            
            var answerQuestionDbo = new AnswerQuestionDbo
            {
                Answer = answerDbo,
                Question = questionDbo,
                Solution = dataObject.Solution
            };

            _context.AnswerThemesDbo.Add(answerQuestionDbo);

            await _context.SaveChangesAsync();

            return questionDbo.Id;
        }

        public async Task<IEnumerable<QuizQuestionDataObject>> GetQuizQuestionsAsync(int themeId)
        {
            var questionsQuery = _context.Questions
                .Include(q => q.Answer)
                .ThenInclude(qa => qa.Answer)
                .Where(q => q.ThemeId == themeId)
                .Select(q => new QuizQuestionDataObject
                {
                    AnswerId = q.Answer.AnswerId,
                    Description = q.Body,
                    Id = q.Id,
                    Name = q.Name,
                    AnswerText = q.Answer.Answer.Title,
                    QuestionType = q.QuestionType,
                    Solution = q.Answer.Solution
                });

            var quizQuestions = await questionsQuery.ToListAsync();

            return quizQuestions;
        }

    }
}
