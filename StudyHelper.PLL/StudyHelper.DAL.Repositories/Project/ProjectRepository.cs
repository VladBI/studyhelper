﻿using StudyHelper.BLL.Interfaces.Models.DataObjects.Project;
using StudyHelper.BLL.Interfaces.Repositories;
using StudyHelper.DAL.Context.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using StudyHelper.DAL.Context.Models;

namespace StudyHelper.DAL.Repositories.Project
{
    public class ProjectRepository: IProjectRepository
    {
        private readonly StudyHelperContext _context;

        private readonly IMapper _mapper;

        public ProjectRepository(StudyHelperContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<int> AddProjectAsync(AddProjectDataObject dataObject)
        {
            var dbEntity = _mapper.Map<ProjectDbo>(dataObject);

            _context.Projects.Add(dbEntity);

            await _context.SaveChangesAsync();

            return dbEntity.Id;
        }

        public async Task<IEnumerable<SimpleProjectDataObject>> GetAllProjectsAsync()
        {
            var projectDboList = _context.Projects.Select(pr => new SimpleProjectDataObject
            {
                Id = pr.Id,
                Description = pr.Description,
                Name = pr.Name
            });

            var projectsList = await projectDboList.ToListAsync();

            return projectsList;

        }

        public async Task<ProjectMainInfoDataObject> GetConcreteProjectAsync(int id)
        {
            var projectQuery = _context.Projects
                //.Where(pr => pr.Id == id)
                .Select(pr => new ProjectMainInfoDataObject
                {
                Id = pr.Id,
                Description = pr.Description,
                Name = pr.Name,
            });

            var project = await projectQuery.FirstOrDefaultAsync(x => x.Id == id);

            project.ThemesCount = await _context.Themes.Where(th => th.ProjectId == id).CountAsync();

            return project;
        }
    }
}
