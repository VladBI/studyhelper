﻿using System.Reflection;
using NUnit.Framework;
using StudyHelper.BLL.Core.Entities.Project;
using StudyHelper.Common.Exceptions;

namespace StudyHelper.Tests.BLL.Core
{
    class ProjectTests
    {
        [Test]
        public void CreateProject_AddNewProjectData_CheckDataIsValidTrue()
        {

            ProjectEntity projectEntity = new ProjectEntity
            {
                Name = "Test",
                Description = "Desc test"
            };

            Assert.That(() => projectEntity.CheckValidData(), Throws.Nothing);
        }

        [Test]
        public void CreateProject_AddNewProjectData_CheckNameIsNotValid()
        {

            ProjectEntity projectEntity = new ProjectEntity
            {
                Name = "Test"
            };

            Assert.Throws(typeof(NotRightPropertyException),
                projectEntity.CheckValidData);
        }

        [Test]
        public void CreateProject_AddNewProjectData_CheckDescIsNotValid()
        {

            ProjectEntity projectEntity = new ProjectEntity
            {
                Description = "Desc test"
            };

            Assert.Throws(typeof(NotRightPropertyException),
                projectEntity.CheckValidData);
        }
    }
}
