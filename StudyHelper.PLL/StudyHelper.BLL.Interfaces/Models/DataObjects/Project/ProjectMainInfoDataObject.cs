﻿using StudyHelper.BLL.Interfaces.Models.DataObjects.Theme;
using System.Collections.Generic;

namespace StudyHelper.BLL.Interfaces.Models.DataObjects.Project
{
    public class ProjectMainInfoDataObject
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int ThemesCount { get; set; }

    }
}
