﻿namespace StudyHelper.BLL.Interfaces.Models.DataObjects.Project
{
    public class SimpleProjectDataObject
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
