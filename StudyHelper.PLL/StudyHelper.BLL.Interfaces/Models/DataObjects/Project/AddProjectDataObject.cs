﻿namespace StudyHelper.BLL.Interfaces.Models.DataObjects.Project
{
    public class AddProjectDataObject
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
