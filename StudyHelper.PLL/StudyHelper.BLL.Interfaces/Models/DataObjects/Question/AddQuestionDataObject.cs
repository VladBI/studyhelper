﻿namespace StudyHelper.BLL.Interfaces.Models.DataObjects.Question
{
    public class AddQuestionDataObject
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int ThemeId { get; set; }

        public string Solution { get; set; }

        public int AnswerId { get; set; }

        public int QuestionType { get; set; }

    }
}
