﻿namespace StudyHelper.BLL.Interfaces.Models.DataObjects.Answer
{
    public class AddAnswerDataObject
    {
        public string Title { get; set; }

        public string AnswerText { get; set; }

        public int[] QuestionTypes { get; set; }

    }
}
