﻿namespace StudyHelper.BLL.Interfaces.Models.DataObjects.Answer
{
    public class SimpleAnswerDataObject
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public int[] QuestionTypes { get; set; }
    }
}
