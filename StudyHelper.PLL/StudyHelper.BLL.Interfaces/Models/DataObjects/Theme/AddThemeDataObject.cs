﻿namespace StudyHelper.BLL.Interfaces.Models.DataObjects.Theme
{
    public class AddThemeDataObject
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int ProjectId { get; set; }
    }
}
