﻿namespace StudyHelper.BLL.Interfaces.Models.DataObjects.Theme
{
    public class SimpleThemeDataObject
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
