﻿namespace StudyHelper.BLL.Interfaces.Models.ViewObjects.Theme
{
    public class SimpleThemeViewObject
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
