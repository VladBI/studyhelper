﻿namespace StudyHelper.BLL.Interfaces.Models.ViewObjects.Theme
{
    public class AddThemeViewObject
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int ProjectId { get; set; }
    }
}
