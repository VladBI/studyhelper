﻿namespace StudyHelper.BLL.Interfaces.Models.ViewObjects.Theme
{
    public class ThemeMainInfoViewObject
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int QuestionsCount { get; set; }
        
        public int ProjectId { get; set; }
    }
}
