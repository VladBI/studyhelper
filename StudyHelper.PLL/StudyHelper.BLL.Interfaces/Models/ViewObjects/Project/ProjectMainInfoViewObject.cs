﻿using StudyHelper.BLL.Interfaces.Models.ViewObjects.Theme;
using System.Collections.Generic;

namespace StudyHelper.BLL.Interfaces.Models.ViewObjects.Project
{
    public class ProjectMainInfoViewObject
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int ThemesCount { get; set; }

        public IEnumerable<SimpleThemeViewObject> Themes { get; set; }
    }
}
