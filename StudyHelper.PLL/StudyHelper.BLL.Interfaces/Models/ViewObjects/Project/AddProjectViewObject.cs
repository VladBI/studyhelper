﻿namespace StudyHelper.BLL.Interfaces.Models.ViewObjects.Project
{
    public class AddProjectViewObject
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
