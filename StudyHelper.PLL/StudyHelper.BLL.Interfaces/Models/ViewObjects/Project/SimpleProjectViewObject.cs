﻿namespace StudyHelper.BLL.Interfaces.Models.ViewObjects.Project
{
    public class SimpleProjectViewObject
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
