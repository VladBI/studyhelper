﻿namespace StudyHelper.BLL.Interfaces.Models.ViewObjects.Answer
{
    public class AddAnswerViewObject
    {
        public string Title { get; set; }

        public string AnswerText { get; set; }

        public int[] QuestionTypes { get; set; }
    }
}
