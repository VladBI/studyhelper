﻿namespace StudyHelper.BLL.Interfaces.Models.ViewObjects.Answer
{
    public class SimpleAnswerViewObject
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public int[] QuestionTypes { get; set; }
    }
}
