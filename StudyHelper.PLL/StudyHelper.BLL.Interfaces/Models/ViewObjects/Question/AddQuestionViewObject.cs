﻿using StudyHelper.BLL.Interfaces.Models.ViewObjects.Answer;

namespace StudyHelper.BLL.Interfaces.Models.ViewObjects.Question
{
    public class AddQuestionViewObject
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Solution { get; set; }

        public int ThemeId { get; set; }

        public int AnswerId { get; set; }

        public AddAnswerViewObject Answer { get; set; }

        public int QuestionType { get; set; }

    }
}
