﻿namespace StudyHelper.BLL.Interfaces.Models.ViewObjects.Question
{
    public class QuizQuestionViewObject
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int QuestionType { get; set; }

        public int AnswerId { get; set; }

        public string AnswerText { get; set; }

        public string Solution { get; set; }
    }
}
