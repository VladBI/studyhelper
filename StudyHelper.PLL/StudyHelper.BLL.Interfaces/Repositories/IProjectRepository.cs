﻿using System.Collections.Generic;
using StudyHelper.BLL.Interfaces.Models.DataObjects.Project;
using System.Threading.Tasks;

namespace StudyHelper.BLL.Interfaces.Repositories
{
    public interface IProjectRepository
    {
        Task<int> AddProjectAsync(AddProjectDataObject dataObject);

        Task<IEnumerable<SimpleProjectDataObject>> GetAllProjectsAsync();

        Task<ProjectMainInfoDataObject> GetConcreteProjectAsync(int id);

    }
}
