﻿using StudyHelper.BLL.Interfaces.Models.DataObjects.Theme;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StudyHelper.BLL.Interfaces.Repositories
{
    public interface IThemeRepository
    {
        Task<int> AddThemeAsync(AddThemeDataObject dataObject);

        Task<IEnumerable<SimpleThemeDataObject>> GetProjectsThemes(int id);

        Task<ThemeMainInfoDataObject> GetConcreteTheme(int id);

    }
}
