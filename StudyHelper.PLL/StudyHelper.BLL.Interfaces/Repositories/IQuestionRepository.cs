﻿using System.Collections.Generic;
using StudyHelper.BLL.Interfaces.Models.DataObjects.Question;
using System.Threading.Tasks;

namespace StudyHelper.BLL.Interfaces.Repositories
{
    public interface IQuestionRepository
    {
        Task<int> AddQuestionAsync(AddQuestionDataObject dataObject);

        Task<IEnumerable<QuizQuestionDataObject>> GetQuizQuestionsAsync(int themeId);
    }
}
