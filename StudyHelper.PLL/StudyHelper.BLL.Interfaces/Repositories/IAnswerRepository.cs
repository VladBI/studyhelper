﻿using System.Collections.Generic;
using StudyHelper.BLL.Interfaces.Models.DataObjects.Answer;
using System.Threading.Tasks;

namespace StudyHelper.BLL.Interfaces.Repositories
{
    public interface IAnswerRepository
    {
        Task<int> AddAnswerAsync(AddAnswerDataObject dataObject);

        Task<IEnumerable<SimpleAnswerDataObject>> GetAllThemesAnswers(int themeId);
    }
}
