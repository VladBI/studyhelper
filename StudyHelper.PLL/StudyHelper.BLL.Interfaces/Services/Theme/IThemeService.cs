﻿using StudyHelper.BLL.Interfaces.Models.ViewObjects.Theme;
using System.Threading.Tasks;

namespace StudyHelper.BLL.Interfaces.Services.Theme
{
    public interface IThemeService
    {
        Task<int> AddThemeAsync(AddThemeViewObject viewObject);

        Task<ThemeMainInfoViewObject> GetConcreteTheme(int id);
    }
}
