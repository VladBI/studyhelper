﻿using StudyHelper.BLL.Interfaces.Models.ViewObjects.Answer;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StudyHelper.BLL.Interfaces.Services.Answer
{
    public interface IAnswersService
    {
        Task<IEnumerable<SimpleAnswerViewObject>> GetAllThemesAnswers(int themeId);
    }
}
