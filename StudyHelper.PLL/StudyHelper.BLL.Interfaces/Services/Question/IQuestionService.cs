﻿using StudyHelper.BLL.Interfaces.Models.ViewObjects.Question;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StudyHelper.BLL.Interfaces.Services.Question
{
    public interface IQuestionService
    {
        Task<int> AddQuestionAsync(AddQuestionViewObject viewObject);

        Task<IEnumerable<QuizQuestionViewObject>> GetQuizQuestionsAsync(int themeId);
    }
}
