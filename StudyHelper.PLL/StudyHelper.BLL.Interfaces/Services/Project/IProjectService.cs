﻿using System.Collections.Generic;
using System.Threading.Tasks;
using StudyHelper.BLL.Interfaces.Models.ViewObjects.Project;

namespace StudyHelper.BLL.Interfaces.Services.Project
{
    public interface IProjectService
    {
        Task<int> AddProjectAsync(AddProjectViewObject viewObject);

        Task<IEnumerable<SimpleProjectViewObject>> GetAllProjectsAsync();

        Task<ProjectMainInfoViewObject> GetConcreteProjectAsync(int id);
    }
}