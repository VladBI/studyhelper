﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace StudyHelper.DAL.Migrations.Migrations
{
    public partial class answer_title : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Answer",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Answer",
                keyColumn: "Id",
                keyValue: 1,
                column: "IsShared",
                value: true);

            migrationBuilder.UpdateData(
                table: "Answer",
                keyColumn: "Id",
                keyValue: 2,
                column: "IsShared",
                value: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Title",
                table: "Answer");

            migrationBuilder.UpdateData(
                table: "Answer",
                keyColumn: "Id",
                keyValue: 1,
                column: "IsShared",
                value: true);

            migrationBuilder.UpdateData(
                table: "Answer",
                keyColumn: "Id",
                keyValue: 2,
                column: "IsShared",
                value: true);
        }
    }
}
