﻿namespace StudyHelper.DAL.Context.Consts
{
    public static class TableNamesConsts
    {
        public const string ProjectTable = "Project";

        public const string ThemeTable = "Theme";

        public const string QuestionTable = "Question";

        public const string AnswerTable = "Answer";

        public const string AnswerQuestionTable = "AnswerQuestion";

    }
}
