﻿using Microsoft.EntityFrameworkCore;
using StudyHelper.DAL.Context.Consts;
using StudyHelper.DAL.Context.Infrastructure.Converters;
using StudyHelper.DAL.Context.Models;
using StudyHelper.DAL.Context.Models.ManyToMany;

namespace StudyHelper.DAL.Context.Context
{
    public class StudyHelperContext : DbContext
    {
        public DbSet<ProjectDbo> Projects { get; set; }

        public DbSet<ThemeDbo> Themes { get; set; }

        public DbSet<QuestionDbo> Questions { get; set; }

        public DbSet<AnswerDbo> Answers { get; set; }

        public DbSet<AnswerQuestionDbo> AnswerThemesDbo { get; set; }


        public StudyHelperContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var intArrayConverter = IntArrayConverter.Convert();
            
            modelBuilder.Entity<ProjectDbo>().ToTable(TableNamesConsts.ProjectTable);
            modelBuilder.Entity<ProjectDbo>().Property(e => e.Id).UseIdentityColumn();
            
            modelBuilder.Entity<ThemeDbo>().ToTable(TableNamesConsts.ThemeTable);
            modelBuilder.Entity<ThemeDbo>().Property(e => e.Id).UseIdentityColumn();
            modelBuilder.Entity<ThemeDbo>()
                .HasOne(op => op.Project)
                .WithMany(i => i.Themes)
                .HasForeignKey(op => op.ProjectId).OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<QuestionDbo>().ToTable(TableNamesConsts.QuestionTable);
            modelBuilder.Entity<QuestionDbo>().Property(e => e.Id).UseIdentityColumn();
            modelBuilder.Entity<QuestionDbo>()
                .HasOne(op => op.Theme)
                .WithMany(i => i.Questions)
                .HasForeignKey(op => op.ThemeId).OnDelete(DeleteBehavior.Restrict);
            
            modelBuilder.Entity<AnswerDbo>().ToTable(TableNamesConsts.AnswerTable);
            modelBuilder.Entity<AnswerDbo>().Property(e => e.Id).UseIdentityColumn();
            modelBuilder.Entity<AnswerDbo>()
                        .Property(e => e.QuestionTypes)
                        .HasConversion(intArrayConverter);
            modelBuilder.Entity<AnswerDbo>()
                .Property(e => e.IsShared)
                .HasDefaultValue(false);

            modelBuilder.Entity<AnswerQuestionDbo>().ToTable(TableNamesConsts.AnswerQuestionTable);
            modelBuilder.Entity<AnswerQuestionDbo>()
                .HasKey(bc => new { bc.AnswerId, bc.QuestionId });
            modelBuilder.Entity<AnswerQuestionDbo>()
                .HasOne(bc => bc.Question)
                .WithOne(b => b.Answer)
                .HasForeignKey<AnswerQuestionDbo>(aq => aq.QuestionId);
            modelBuilder.Entity<AnswerQuestionDbo>()
                .HasOne(bc => bc.Answer)
                .WithMany(c => c.Questions)
                .HasForeignKey(bc => bc.AnswerId);

            modelBuilder.Entity<AnswerDbo>().HasData(new AnswerDbo
            {
                Id = 1,
                AnswerText = "Yes",
                QuestionTypes = new[]{4},
                IsShared = true
            });
            modelBuilder.Entity<AnswerDbo>().HasData(new AnswerDbo
            {
                Id = 2,
                AnswerText = "No",
                QuestionTypes = new[] { 4 },
                IsShared = true
            });
        }
    }
}
