﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using System.Linq;

namespace StudyHelper.DAL.Context.Infrastructure.Converters
{
    public static class IntArrayConverter
    {
        public static ValueConverter<int[], string> Convert()
            => new ValueConverter<int[], string>(
                v => string.Join(";", v),
                v => v.Split(";", StringSplitOptions.RemoveEmptyEntries)
                .Select(val => int.Parse(val))
                .ToArray());
    }
}
