﻿using System.Collections.Generic;

namespace StudyHelper.DAL.Context.Models
{
    public class ProjectDbo
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public IEnumerable<ThemeDbo> Themes { get; set; }
    }
}
