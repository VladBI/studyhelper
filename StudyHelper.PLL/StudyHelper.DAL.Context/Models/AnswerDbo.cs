﻿using System.Collections.Generic;
using StudyHelper.DAL.Context.Models.ManyToMany;

namespace StudyHelper.DAL.Context.Models
{
    public class AnswerDbo
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string AnswerText { get; set; }

        public bool IsShared { get; set; }

        public IEnumerable<AnswerQuestionDbo> Questions { get; set; }

        public int[] QuestionTypes { get; set; }
    }
}
