﻿namespace StudyHelper.DAL.Context.Models.ManyToMany
{
    public class AnswerQuestionDbo
    {
        public int AnswerId { get; set; }

        public AnswerDbo Answer { get; set; }

        public int QuestionId { get; set; }

        public QuestionDbo Question { get; set; }

        public string Solution { get; set; }

    }
}
