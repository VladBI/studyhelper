﻿using System.Collections.Generic;
using StudyHelper.DAL.Context.Models.ManyToMany;

namespace StudyHelper.DAL.Context.Models
{
    public class ThemeDbo
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
        
        public string Description { get; set; }

        public int ProjectId { get; set; }
        
        public ProjectDbo Project { get; set; }

        public IEnumerable<QuestionDbo> Questions { get; set; }

    }
}
