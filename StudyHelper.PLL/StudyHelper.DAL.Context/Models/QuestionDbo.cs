﻿using System.Collections.Generic;
using StudyHelper.DAL.Context.Models.ManyToMany;

namespace StudyHelper.DAL.Context.Models
{
    public class QuestionDbo
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Body { get; set; }

        public int ThemeId { get; set; }

        public ThemeDbo Theme { get; set; }
        
        public int QuestionType { get; set; }

        public AnswerQuestionDbo Answer { get; set; }
    }
}
