﻿namespace StudyHelper.BLL.Core.Enum
{
    public enum QuestionType
    {
        SingleAnswer = 1,
        
        MultipleAnswer = 2,
        
        InputBlank = 3,
        
        YesNo = 4 
    }
}
