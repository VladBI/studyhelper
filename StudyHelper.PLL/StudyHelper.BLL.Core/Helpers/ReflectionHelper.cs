﻿using System;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;

namespace StudyHelper.BLL.Core.Helpers
{
    public static class ReflectionHelper
    {
        public static PropertyInfo[] GetClassProperties<T>(T classForReflection) where T : new()
        {
            if (classForReflection == null)
            {
                throw new NullReferenceException("class for reflection was null");
            }

            var properties = classForReflection.GetType().GetProperties();

            return properties;
        }
    }
}
