﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using StudyHelper.BLL.Core.Attributes.Validation;

namespace StudyHelper.BLL.Core.Helpers
{
    public static class ValidationHelper
    {
        public static bool IsObjectValid<T>(PropertyInfo[] properties, T objectForCheck) where T : new()
        {

            foreach (var property in properties)
            {
                var propValue = property.GetValue(objectForCheck);
                var propAttributes = property.CustomAttributes;
                var propertyType = property.PropertyType.Name;

                switch (propertyType)
                {
                    case "String":
                        var stringValue = (string) propValue;
                        if (propAttributes
                            .Any(x => x.AttributeType == typeof(RequiredAttribute)
                                      && string.IsNullOrEmpty(stringValue)))
                        {
                            return false;
                        }

                        break;

                    case "Int32":
                        var intValue = (int) propValue;
                        if (propAttributes
                            .Any(x => x.AttributeType == typeof(RequiredIdAttribute)
                                      && intValue <= 0))
                        {
                            return false;
                        }

                        break;
                }
            }

            return true;
        }
    }
}
