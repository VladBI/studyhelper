﻿using System.ComponentModel.DataAnnotations;
using StudyHelper.BLL.Core.Attributes.Validation;

namespace StudyHelper.BLL.Core.Entities.Theme
{
    public class ThemeEntity: BaseEntity
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [RequiredIdAttribute]
        public int ProjectId { get; set; }

    }
}
