﻿using System.ComponentModel.DataAnnotations;

namespace StudyHelper.BLL.Core.Entities.Project
{
    public class ProjectEntity: BaseEntity
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        
    }
}
