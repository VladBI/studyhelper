﻿using StudyHelper.BLL.Core.Enum;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace StudyHelper.BLL.Core.Entities.Answer
{
    public class AnswerEntity: BaseEntity
    {
        [Required]
        public string Title { get; set; }

        [Required]
        public string AnswerText { get; set; }

        [Required]
        public string Solution { get; set; }

        public IEnumerable<QuestionType> QuestionTypes { get; set; }

    }
}
