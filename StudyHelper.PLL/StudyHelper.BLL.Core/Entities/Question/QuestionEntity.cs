﻿using System.ComponentModel.DataAnnotations;
using StudyHelper.BLL.Core.Attributes.Validation;
using StudyHelper.BLL.Core.Entities.Answer;
using StudyHelper.BLL.Core.Enum;

namespace StudyHelper.BLL.Core.Entities.Question
{
    public class QuestionEntity: BaseEntity
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [RequiredId]
        public int ThemeId { get; set; }

        public int AnswerId { get; set; }

        public AnswerEntity Answer { get; set; }

        public QuestionType QuestionType { get; set; }

    }
}
