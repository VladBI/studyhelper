﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.InteropServices;
using StudyHelper.BLL.Core.Helpers;
using StudyHelper.Common.Exceptions;

namespace StudyHelper.BLL.Core.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }

        public void CheckValidData()
        {
            var properties = ReflectionHelper.GetClassProperties(this);
            if (!ValidationHelper.IsObjectValid(properties, this))
            {
                throw new NotRightPropertyException();
            }
        }
    }
}
