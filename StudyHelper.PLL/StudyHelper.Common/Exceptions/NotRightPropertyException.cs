﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;

namespace StudyHelper.Common.Exceptions
{
    public class NotRightPropertyException: Exception
    {
        public NotRightPropertyException() : base(CreateExceptionMessage(string.Empty))
        {
        }

        public NotRightPropertyException(string propName) :base(CreateExceptionMessage(propName))
        {
        }

        static string CreateExceptionMessage(string propertyName)
        {
            string errorMessage = $"Property {propertyName} is not valid";

            return errorMessage;
        }
    }
}
