﻿using AutoMapper;
using StudyHelper.BLL.Core.Entities.Answer;
using StudyHelper.BLL.Core.Entities.Question;
using StudyHelper.BLL.Interfaces.Models.DataObjects.Answer;
using StudyHelper.BLL.Interfaces.Models.DataObjects.Question;
using StudyHelper.BLL.Interfaces.Models.ViewObjects.Question;
using StudyHelper.BLL.Interfaces.Repositories;
using StudyHelper.BLL.Interfaces.Services.Question;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StudyHelper.BLL.Services.Question
{
    public class QuestionService: IQuestionService
    {
        private readonly IQuestionRepository _questionRepository;

        private readonly IAnswerRepository _answerRepository;

        private readonly IMapper _mapper;

        public QuestionService(IQuestionRepository questionRepository, IAnswerRepository answerRepository, IMapper mapper)
        {
            _questionRepository = questionRepository;
            _answerRepository = answerRepository;
            _mapper = mapper;
        }

        public async Task<int> AddQuestionAsync(AddQuestionViewObject viewObject)
        {
            var questionEntity = _mapper.Map<QuestionEntity>(viewObject);
            questionEntity.CheckValidData();
            
            if (questionEntity.Answer != null)
            {
                var answerEntity = _mapper.Map<AnswerEntity>(questionEntity.Answer);
                answerEntity.Solution = viewObject.Solution;

                answerEntity.CheckValidData();

                var answerDataObject = _mapper.Map<AddAnswerDataObject>(answerEntity);

                var answerId = await _answerRepository.AddAnswerAsync(answerDataObject);

                questionEntity.AnswerId = answerId;
            }

            var questionDataObject = _mapper.Map<AddQuestionDataObject>(questionEntity);
            questionDataObject.Solution = viewObject.Solution;

            var questionId = await _questionRepository.AddQuestionAsync(questionDataObject);

            return questionId;
        }

        public async Task<IEnumerable<QuizQuestionViewObject>> GetQuizQuestionsAsync(int themeId)
        {
            var dataObjectList = await _questionRepository.GetQuizQuestionsAsync(themeId);
            var viewObjectList = _mapper.Map<IEnumerable<QuizQuestionViewObject>>(dataObjectList);

            return viewObjectList;
        }
    }
}
