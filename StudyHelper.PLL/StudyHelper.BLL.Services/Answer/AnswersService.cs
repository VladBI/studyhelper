﻿using AutoMapper;
using StudyHelper.BLL.Interfaces.Models.ViewObjects.Answer;
using StudyHelper.BLL.Interfaces.Repositories;
using StudyHelper.BLL.Interfaces.Services.Answer;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StudyHelper.BLL.Services.Answer
{
    public class AnswersService: IAnswersService
    {
        private readonly IAnswerRepository _answerRepository;

        private readonly IMapper _mapper;

        public AnswersService(IAnswerRepository answerRepository, IMapper mapper)
        {
            _answerRepository = answerRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<SimpleAnswerViewObject>> GetAllThemesAnswers(int themeId)
        {
            var answersDo = await _answerRepository.GetAllThemesAnswers(themeId);
            var answersVo = _mapper.Map<IEnumerable<SimpleAnswerViewObject>>(answersDo);

            return answersVo;
        }
    }
}
