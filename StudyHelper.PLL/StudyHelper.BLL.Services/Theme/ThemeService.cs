﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using StudyHelper.BLL.Core.Entities.Theme;
using StudyHelper.BLL.Interfaces.Models.DataObjects.Theme;
using StudyHelper.BLL.Interfaces.Models.ViewObjects.Theme;
using StudyHelper.BLL.Interfaces.Repositories;
using StudyHelper.BLL.Interfaces.Services.Theme;

namespace StudyHelper.BLL.Services.Theme
{
    public class ThemeService:IThemeService
    {
        private readonly IThemeRepository _themeRepository;

        private readonly IMapper _mapper;

        public ThemeService(IThemeRepository themeRepository, IMapper mapper)
        {
            _themeRepository = themeRepository;
            _mapper = mapper;
        }

        public async Task<int> AddThemeAsync(AddThemeViewObject viewObject)
        {
            var themeEntity = _mapper.Map<ThemeEntity>(viewObject);
            themeEntity.CheckValidData();

            var dataObject = _mapper.Map<AddThemeDataObject>(themeEntity);
            var id = await _themeRepository.AddThemeAsync(dataObject);

            return id;
        }

        public async Task<ThemeMainInfoViewObject> GetConcreteTheme(int id)
        {
            var themeDo = await _themeRepository.GetConcreteTheme(id);
            var themeVo = _mapper.Map<ThemeMainInfoViewObject>(themeDo);

            return themeVo;
        }
    }
}
