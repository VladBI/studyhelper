﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using StudyHelper.BLL.Core.Entities.Project;
using StudyHelper.BLL.Interfaces.Models.DataObjects.Project;
using StudyHelper.BLL.Interfaces.Models.ViewObjects.Project;
using StudyHelper.BLL.Interfaces.Models.ViewObjects.Theme;
using StudyHelper.BLL.Interfaces.Repositories;
using StudyHelper.BLL.Interfaces.Services.Project;

namespace StudyHelper.BLL.Services.Project
{
    public class ProjectService : IProjectService
    {
        private readonly IMapper _mapper;
        private readonly IProjectRepository _projectRepository;
        private readonly IThemeRepository _themeRepository;

        public ProjectService(IMapper mapper, IProjectRepository projectRepository, IThemeRepository themeRepository)
        {
            _mapper = mapper;
            _projectRepository = projectRepository;
            _themeRepository = themeRepository;
        }

        public async Task<int> AddProjectAsync(AddProjectViewObject viewObject)
        {
            var projectEntity = _mapper.Map<ProjectEntity>(viewObject);
            projectEntity.CheckValidData();

            var dataObject = _mapper.Map<AddProjectDataObject>(projectEntity);

            var projectId = await _projectRepository.AddProjectAsync(dataObject);

            return projectId;
        }

        public async Task<IEnumerable<SimpleProjectViewObject>> GetAllProjectsAsync()
        {
            var dataObjectsList = await _projectRepository.GetAllProjectsAsync();

            var viewObjectsList = _mapper.Map<IEnumerable<SimpleProjectViewObject>>(dataObjectsList);

            return viewObjectsList;
        }

        public async Task<ProjectMainInfoViewObject> GetConcreteProjectAsync(int id)
        {
            var projectDo = await _projectRepository.GetConcreteProjectAsync(id);

            var projectVo = _mapper.Map<ProjectMainInfoViewObject>(projectDo);

            var themesDoList = await _themeRepository.GetProjectsThemes(id);

            var themesVoList = _mapper.Map<IEnumerable<SimpleThemeViewObject>>(themesDoList);

            projectVo.Themes = themesVoList;

            return projectVo;
        }
    }
}
