﻿using System.Collections.Generic;

namespace StudyHelper.PLL.Infrastructure.Models
{
    public class DbSettings
    {
        public Dictionary<string, string> ConnectionStrings { get; set; }
        
        public string MigrationAssembly { get; set; }
        
        public string CurrentConnectionName { get; set; }
    }
}
