﻿using AutoMapper;
using StudyHelper.BLL.Core.Entities.Answer;
using StudyHelper.BLL.Core.Entities.Project;
using StudyHelper.BLL.Core.Entities.Question;
using StudyHelper.BLL.Core.Entities.Theme;
using StudyHelper.BLL.Interfaces.Models.DataObjects.Answer;
using StudyHelper.BLL.Interfaces.Models.DataObjects.Project;
using StudyHelper.BLL.Interfaces.Models.DataObjects.Question;
using StudyHelper.BLL.Interfaces.Models.DataObjects.Theme;
using StudyHelper.BLL.Interfaces.Models.ViewObjects.Answer;
using StudyHelper.BLL.Interfaces.Models.ViewObjects.Project;
using StudyHelper.BLL.Interfaces.Models.ViewObjects.Question;
using StudyHelper.BLL.Interfaces.Models.ViewObjects.Theme;
using StudyHelper.DAL.Context.Models;
using StudyHelper.PLL.Models.Answer;
using StudyHelper.PLL.Models.Project;
using StudyHelper.PLL.Models.Question;
using StudyHelper.PLL.Models.Theme;

namespace StudyHelper.PLL.Infrastructure.AutoMapper
{
    class MainProfile: Profile
    {
        public MainProfile()
        {
            CreateMap<AddProjectViewModel, AddProjectViewObject>();
            CreateMap<AddProjectViewObject, ProjectEntity>();
            CreateMap<ProjectEntity, AddProjectDataObject>();
            CreateMap<AddProjectDataObject, ProjectDbo>();
            CreateMap<SimpleProjectDataObject, SimpleProjectViewObject>();
            CreateMap<SimpleProjectViewObject, SimpleProjectViewModel>();
            CreateMap<ProjectMainInfoDataObject, ProjectMainInfoViewObject>();
            CreateMap<ProjectMainInfoViewObject, ProjectMainInfoViewModel>();

            CreateMap<AddThemeViewModel, AddThemeViewObject>();
            CreateMap<AddThemeViewObject, ThemeEntity>();
            CreateMap<ThemeEntity, AddThemeDataObject>();
            CreateMap<AddThemeDataObject, ThemeDbo>();
            CreateMap<SimpleThemeDataObject, SimpleThemeViewObject>();
            CreateMap<SimpleThemeViewObject, SimpleThemeViewModel>();
            CreateMap<ThemeMainInfoDataObject, ThemeMainInfoViewObject>();
            CreateMap<ThemeMainInfoViewObject, ThemeMainInfoViewModel>();

            CreateMap<AddQuestionViewModel, AddQuestionViewObject>();
            CreateMap<AddQuestionViewObject, QuestionEntity>();
            CreateMap<QuestionEntity, AddQuestionDataObject>();
            CreateMap<AddQuestionDataObject, QuestionDbo>()
                .ForMember(dest => dest.Body, opt => opt.MapFrom(s => s.Description));
            CreateMap<QuizQuestionDataObject, QuizQuestionViewObject>();
            CreateMap<QuizQuestionViewObject, QuizQuestionViewModel>();

            CreateMap<AddAnswerViewModel, AddAnswerViewObject>();
            CreateMap<AddAnswerViewObject, AnswerEntity>();
            CreateMap<AnswerEntity, AddAnswerDataObject>();
            CreateMap<AddAnswerDataObject, AnswerDbo>();
            CreateMap<SimpleAnswerDataObject, SimpleAnswerViewObject>();
            CreateMap<SimpleAnswerViewObject, SimpleAnswerViewModel>();

        }
    }
}
