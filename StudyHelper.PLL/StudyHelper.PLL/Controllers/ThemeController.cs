﻿using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using StudyHelper.BLL.Interfaces.Models.ViewObjects.Theme;
using StudyHelper.BLL.Interfaces.Services.Theme;
using StudyHelper.PLL.Models.Theme;
using System.Threading.Tasks;
using StudyHelper.BLL.Interfaces.Services.Answer;
using StudyHelper.PLL.Models.Answer;

namespace StudyHelper.PLL.Controllers
{
    [Route("api/themes")]
    public class ThemeController : Controller
    {
        private readonly IMapper _mapper;

        private readonly IThemeService _themeService;

        private readonly IAnswersService _answersService;

        public ThemeController(IMapper mapper, IThemeService themeService, IAnswersService answersService)
        {
            _mapper = mapper;
            _themeService = themeService;
            _answersService = answersService;
        }

        [HttpPost]
        public async Task<IActionResult> AddTheme(AddThemeViewModel viewModel)
        {
            var viewObject = _mapper.Map<AddThemeViewObject>(viewModel);
            var themeId = await _themeService.AddThemeAsync(viewObject);

            return Ok(themeId);
        }

        [Route("{id:int}")]
        public async Task<IActionResult> GetConcreteTheme(int id)
        {
            var viewObject = await _themeService.GetConcreteTheme(id);
            var viewModel = _mapper.Map<ThemeMainInfoViewModel>(viewObject);

            return Ok(viewModel);
        }

        [Route("answers/{themeId:int}")]
        public async Task<IActionResult> GetThemesAnswers(int themeId)
        {
            var viewObject = await _answersService.GetAllThemesAnswers(themeId);
            var viewModel = _mapper.Map<IEnumerable<SimpleAnswerViewModel>>(viewObject);

            return Ok(viewModel);
        }
    }
}
