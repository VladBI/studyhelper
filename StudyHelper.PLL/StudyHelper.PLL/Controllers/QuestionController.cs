﻿using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using StudyHelper.BLL.Interfaces.Models.ViewObjects.Question;
using StudyHelper.BLL.Interfaces.Services.Question;
using StudyHelper.PLL.Models.Question;
using System.Threading.Tasks;

namespace StudyHelper.PLL.Controllers
{
    [Route("api/questions")]
    public class QuestionController: Controller
    {
        private readonly IMapper _mapper;

        private readonly IQuestionService _questionService;

        public QuestionController(IMapper mapper, IQuestionService questionService)
        {
            _mapper = mapper;
            _questionService = questionService;
        }

        [HttpPost]
        public async Task<IActionResult> AddQuestion(AddQuestionViewModel viewModel)
        {
            var viewObject = _mapper.Map<AddQuestionViewObject>(viewModel);
            var questionId = await _questionService.AddQuestionAsync(viewObject);

            return Ok(questionId);
        }

        [Route("themeId:int")]
        public async Task<IActionResult> GetQuizQuestions(int themeId)
        {
            var quizQuestionsVo = await _questionService.GetQuizQuestionsAsync(themeId);
            var quizQuestionsVm = _mapper.Map<IEnumerable<QuizQuestionViewModel>>(quizQuestionsVo);

            return Ok(quizQuestionsVm);
        }
    }
}
