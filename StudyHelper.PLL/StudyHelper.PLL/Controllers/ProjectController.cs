﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using StudyHelper.BLL.Interfaces.Models.ViewObjects.Project;
using StudyHelper.BLL.Interfaces.Services.Project;
using StudyHelper.PLL.Models.Project;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StudyHelper.PLL.Controllers
{
    [Route("api/projects")]
    public class ProjectController : Controller
    {
        private readonly IMapper _mapper;

        private readonly IProjectService _projectService;

        public ProjectController(IMapper mapper, IProjectService projectService)
        {
            _mapper = mapper;
            _projectService = projectService;
        }

        [HttpPost]
        public async Task<IActionResult> AddProject(AddProjectViewModel viewModel)
        {
            var viewObject = _mapper.Map<AddProjectViewObject>(viewModel);
            var projectId = await _projectService.AddProjectAsync(viewObject);

            return Ok(projectId);
        }

        public async Task<IActionResult> GetAllProjects()
        {
            var viewObjectsList = await _projectService.GetAllProjectsAsync();
            var viewModelsList = _mapper.Map<IEnumerable<SimpleProjectViewModel>>(viewObjectsList);

            return Ok(viewModelsList);
        }

        [Route("{id:int}")]
        public async Task<IActionResult> GetConcreteProject(int id)
        {
            var viewObject = await _projectService.GetConcreteProjectAsync(id);
            var viewModel = _mapper.Map<ProjectMainInfoViewModel>(viewObject);

            return Ok(viewModel);
        }
    }
}
