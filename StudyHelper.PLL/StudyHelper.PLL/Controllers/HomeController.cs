﻿using Microsoft.AspNetCore.Mvc;

namespace StudyHelper.PLL.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}