using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using StudyHelper.BLL.Interfaces.Repositories;
using StudyHelper.BLL.Interfaces.Services.Answer;
using StudyHelper.BLL.Interfaces.Services.Project;
using StudyHelper.BLL.Interfaces.Services.Question;
using StudyHelper.BLL.Interfaces.Services.Theme;
using StudyHelper.BLL.Services.Answer;
using StudyHelper.BLL.Services.Project;
using StudyHelper.BLL.Services.Question;
using StudyHelper.BLL.Services.Theme;
using StudyHelper.DAL.Context.Context;
using StudyHelper.DAL.Repositories.Answer;
using StudyHelper.DAL.Repositories.Project;
using StudyHelper.DAL.Repositories.Question;
using StudyHelper.DAL.Repositories.Theme;
using StudyHelper.PLL.Infrastructure.AutoMapper;
using StudyHelper.PLL.Infrastructure.Const;
using StudyHelper.PLL.Infrastructure.Models;

namespace StudyHelper.PLL
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddAutoMapper(typeof(MainProfile));

            var dbSettingsSection = Configuration.GetSection(ConfigSectionsNames.DbSettings);
            services.Configure<DbSettings>(dbSettingsSection);

            var dbSettings = dbSettingsSection.Get<DbSettings>();

            services.AddDbContext<StudyHelperContext>(opts =>
                opts.UseSqlServer(dbSettings.ConnectionStrings[dbSettings.CurrentConnectionName],
                    x => x.MigrationsAssembly(dbSettings.MigrationAssembly))
            );
            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });

            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<IThemeService, ThemeService>();
            services.AddScoped<IQuestionService, QuestionService>();
            services.AddScoped<IAnswersService, AnswersService>();
            services.AddScoped<IProjectRepository, ProjectRepository>();
            services.AddScoped<IThemeRepository, ThemeRepository>();
            services.AddScoped<IQuestionRepository, QuestionRepository>();
            services.AddScoped<IAnswerRepository, AnswerRepository>();

            services.AddSwaggerDocument();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            if (!env.IsDevelopment())
            {
                app.UseSpaStaticFiles();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseOpenApi();
            app.UseSwaggerUi3();

            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }
    }
}
