﻿namespace StudyHelper.PLL.Models.Theme
{
    public class SimpleThemeViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
