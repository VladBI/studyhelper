﻿using System.ComponentModel.DataAnnotations;

namespace StudyHelper.PLL.Models.Theme
{
    public class AddThemeViewModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public int ProjectId { get; set; }
    }
}
