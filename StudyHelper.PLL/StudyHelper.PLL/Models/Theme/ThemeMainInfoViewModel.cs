﻿namespace StudyHelper.PLL.Models.Theme
{
    public class ThemeMainInfoViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int QuestionsCount { get; set; }

        public int ProjectId { get; set; }
    }
}
