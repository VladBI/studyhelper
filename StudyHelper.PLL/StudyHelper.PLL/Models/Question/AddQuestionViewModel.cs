﻿using StudyHelper.BLL.Interfaces.Models.ViewObjects.Answer;
using StudyHelper.PLL.Models.Answer;

namespace StudyHelper.PLL.Models.Question
{
    public class AddQuestionViewModel
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int ThemeId { get; set; }

        public string Solution { get; set; }

        public int AnswerId { get; set; }

        public AddAnswerViewModel Answer { get; set; }

        public int QuestionType { get; set; }

    }
}
