﻿namespace StudyHelper.PLL.Models.Answer
{
    public class SimpleAnswerViewModel
    {
        public int Id { get; set; }

        public string AnswerText { get; set; }

        public int[] QuestionTypes { get; set; }
    }
}
