﻿namespace StudyHelper.PLL.Models.Answer
{
    public class AddAnswerViewModel
    {
        public string Title { get; set; }

        public string AnswerText { get; set; }

        public int[] QuestionTypes { get; set; }
    }
}
