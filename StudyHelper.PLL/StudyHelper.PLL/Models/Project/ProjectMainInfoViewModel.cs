﻿using StudyHelper.PLL.Models.Theme;
using System.Collections.Generic;

namespace StudyHelper.PLL.Models.Project
{
    public class ProjectMainInfoViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int ThemesCount { get; set; }

        public IEnumerable<SimpleThemeViewModel> Themes { get; set; }
    }
}
