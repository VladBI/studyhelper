﻿using System.ComponentModel.DataAnnotations;

namespace StudyHelper.PLL.Models.Project
{
    public class AddProjectViewModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }
    }
}
